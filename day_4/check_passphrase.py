input = open('input.txt', 'r')

valid = 0
invalid = 0

for line in input:
	list = line.split()
	is_invalid = False
	for i in range(0, len(list)):
		check = list[i]
		for n in range(0, len(list)):
			if i >= n:
				pass
			else:
				if check == list[n]:
					is_invalid = True
				else:
					pass
	if is_invalid:
		invalid += 1
	else:
		valid += 1
print("Valid passphrases: " + str(valid) + " Invalid passphrases: " + str(invalid))