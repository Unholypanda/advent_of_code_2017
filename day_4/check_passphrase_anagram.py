input = open('input.txt', 'r')

valid = 0
invalid = 0

for line in input:
	list = line.split()
	is_invalid = False
	for i in range(0, len(list)):
		check = list[i]
		for n in range(0, len(list)):
			if i >= n:
				pass
			else:
				if check == list[n]:
					is_invalid = True
				elif len(check) == len(list[n]):
					letter_match = True
					for c in range(0, len(check)):
						if check[c] not in list[n]:
							letter_match = False
						else:
							pass
					if letter_match:
						is_invalid = True
				else:
					pass
	if is_invalid:
		invalid += 1
	else:
		valid += 1
print("Valid passphrases: " + str(valid) + " Invalid passphrases: " + str(invalid))