input = open('input.txt', 'r')

maze = []

for line in input:
    maze.append(int(line))

pos = 0
jumps = 0

out_of_maze = False

while not out_of_maze:
    try:
        # print(maze)
        # print("Jumps: " + str(jumps))
        # print("Position: " + str(pos))
        old_pos = pos
        new_pos = pos + maze[pos]
        pos = new_pos
        if maze[old_pos] >= 3:
            maze[old_pos] -= 1
        else:
           maze[old_pos] += 1
        jumps += 1
    except:
        out_of_maze = True

print(jumps)

    
