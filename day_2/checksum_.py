file = open('spreadsheet.txt', 'r')

checksum = 0
for line in file:
	min = 0
	max = 0
	list = line.split()
	for number in list:
		number = int(number)
		if min == 0 or number < min:
			min = number
		if max == 0 or number > max:
			max 	= number
	checksum += (max - min)
	print("Row checksum " + str(max - min) + ".Min - " + str(min) + ". Max - " + str(max))
print(checksum)